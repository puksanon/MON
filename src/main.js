import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import { store }      from './store'
import vuetify from "./plugins/vuetify";
import VueRx from "vue-rx";
import VueAxios from "vue-axios";
import axios from "axios";
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(VueSweetalert2);
Vue.use(VueAxios, axios);
Vue.use(VueRx);
Vue.config.productionTip = false;
Vue.$http = axios;

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");

import { http } from "../config/http";
import { Observable } from "rxjs";
import axios from "axios";
import authHeader from '../config/request.header'
export default {
  get_user: () => {
    return new Observable(observer => {
      axios
        .get(
            `${http}/users/query?offset=0&limit=9999999` ,{ headers: authHeader() }
        )
        .then(response => {
          observer.next(response.data);
        })
        .catch(error => {
          observer.error(error);
        });
    });
  },

  get_user_profile: () => {
    return new Observable(observer => {
      axios
        .get(
            `${http}/users` ,{ headers: authHeader() }
        )
        .then(response => {
          observer.next(response.data);
        })
        .catch(error => {
          localStorage.clear()
          this.$router.go(this.$router.currentRoute)
          observer.error(error);
        })
    });
  },

  update_user: (user) => {
    return new Observable(observer => {
      axios
        .put(
            `${http}/users/alter`, user ,{ headers: authHeader() }
        )
        .then(response => {
          observer.next(response.data);
        })
        .catch(error => {
          observer.error(error);
        });
    });
  },

  delete_user: (user) => {
    return new Observable(observer => {
      axios
        .delete(
            `${http}/users/${user.user_id}`,{ headers: authHeader() }
        )
        .then(response => {
          observer.next(response.data);
        })
        .catch(error => {
          observer.error(error);
        });
    });
  }
};

import { http } from "../config/http";
import { Observable } from "rxjs";
import axios from "axios";
import authHeader from '../config/request.header'
export default {
  get_promotion: () => {
    return new Observable(observer => {
      axios
        .get(
            `${http}/promotions?offset=0&limit=9999999` ,{ headers: authHeader() }
        )
        .then(response => {
          observer.next(response.data);
        })
        .catch(error => {
          observer.error(error);
        });
    });
  },

  create_promotion: (promotion) => {
    return new Observable(observer => {
      axios
        .post(
            `${http}/promotions`, promotion ,{ headers: authHeader() }
        )
        .then(response => {
          observer.next(response.data);
        })
        .catch(error => {
          observer.error(error);
        });
    });
  },

  update_promotion: (promotion_id , promotion_d) => {
    return new Observable(observer => {
      axios
        .put(
            `${http}/promotions/${promotion_id}`, promotion_d ,{ headers: authHeader() }
        )
        .then(response => {
          observer.next(response.data);
        })
        .catch(error => {
          observer.error(error);
        });
    });
  },

  get_promotion_product_list : (code) => {
    return new Observable(observer => {
      axios
        .get(
            `${http}/promotions/product?limit=9999&promocode_id=${code}`,{ headers: authHeader() }
        )
        .then(response => {
          observer.next(response.data);
        })
        .catch(error => {
          observer.error(error);
        });
    });
  }
};

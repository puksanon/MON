import { http } from "../config/http";
import { Observable } from "rxjs";
import axios from "axios";

export default {
  sign_in: (data) => {
    return new Observable(observer => {
      axios
        .post(`${http}/users/signin`, data)
        .then(response => {
          observer.next(response.data);
        })
        .catch(error => {
          observer.error(error);
        });
    });
  },

  register: (data) => {
    return new Observable(observer => {
      axios
        .post(`${http}/users/register`, data)
        .then(response => {
          observer.next(response.data);
        })
        .catch(error => {
          observer.error(error);
        });
    });
  }
};

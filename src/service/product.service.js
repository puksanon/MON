import { http } from "../config/http";
import { Observable } from "rxjs";
import axios from "axios";
import authHeader from '../config/request.header'

export default {
  get_product: (limit) => {
    return new Observable(observer => {
      axios
        .get(`${http}/products?offset=0&limit=${limit.limit}`)
        .then(response => {
          observer.next(response.data);
        })
        .catch(error => {
          observer.error(error);
        });
    });
  },

  get_product_detail: (p_id) => {
    return new Observable(observer => {
      axios
        .get(`${http}/products?offset=0&limit=0&id=${p_id}`)
        .then(response => {
          observer.next(response.data);
        })
        .catch(error => {
          observer.error(error);
        });
    });
  },

  delete_product: (limit) => {
    return new Observable(observer => {
      axios
        .get(`${http}/products?offset=0&limit=${limit.limit}`)
        .then(response => {
          observer.next(response.data);
        })
        .catch(error => {
          observer.error(error);
        });
    });
  },

  post_category: (cate) => {
    return new Observable(observer => {
      axios
        .post(`${http}/products/categories`, cate ,{ headers: authHeader() })
        .then(response => {
          observer.next(response.data);
        })
        .catch(error => {
          observer.error(error);
        });
    });
  },

  create_product: (body) =>{
    return new Observable(observer => {
      axios
        .post(`${http}/products`, body ,{ headers: authHeader() })
        .then(response => {
          observer.next(response.data);
        })
        .catch(error => {
          observer.error(error);
        });
    });
  },

  create_product_images: (formdata , product_id) =>{
    return new Observable(observer => {
      axios
        .post(`${http}/products/images?product_id=${product_id}`, formdata ,{ headers: authHeader() })
        .then(response => {
          observer.next(response.data);
        })
        .catch(error => {
          observer.error(error);
        });
    });
  },
};

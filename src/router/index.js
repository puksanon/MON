import Vue from "vue";
import VueRouter from "vue-router";
import { store } from '../store';
import service from '../service/user.service'
Vue.use(VueRouter);

const Home = () => import("@/views/pages/Home.vue");
const NotFound = () => import("@/views/pages/NotFound.vue");
const Product = () => import("@/views/pages/Product.vue");
const ProductDetail = () => import("@/views/pages/ProductDetail.vue");
const Customize = () => import("@/views/pages/Customize.vue");
const Cart = () => import("@/views/pages/Cart.vue");
const About = () => import("@/views/pages/About.vue");
const SignIn = () => import("@/views/auth/SignIn.vue");
const Register = () => import("@/views/auth/Register.vue");
const Admin_Dashboard = () => import("@/views/admin/dashboard/dashboard.vue");
const Product_list = () => import("@/views/admin/product/ManageProduct.vue");
const Order_list = () => import("@/views/admin/order/ListOrder.vue");
const AddCustomProductList = () =>
  import("@/views/admin/product/AddCustomProductList.vue");
const NewsPages = () => import("@/views/pages/News.vue");
const ManageUsers = () =>
  import("@/views/admin/manage_users/ManageUserList.vue");
const categories_list = () => import('@/views/admin/product/Categories.vue')
const manage_promotion = () => import('@/views/admin/promotion/manage_promotion.vue')
const manage_promotion_product = () => import('@/views/admin/promotion/manage_product_promotion.vue')
const router =  new VueRouter({
  mode: "history",
  routes :[
  {
    path: "*",
    component: NotFound
  },
  {
    path: "*",
    redirect: "/404"
  },
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/About",
    name: "About",
    component: About
  },
  {
    path: "/Cart",
    name: "Cart",
    component: Cart,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/Product",
    name: "Product",
    component: Product
  },
  {
    path: "/News",
    name: "News",
    component: NewsPages
  },
  {
    path: "/ProductDetail/:product_id",
    name: "ProductDetail",
    component: ProductDetail,
    props: true
  },
  {
    path: "/Product/Customize",
    name: "Customize",
    component: Customize
  },
  {
    path: "/Auth/SignIn",
    name: "SignIn",
    component: SignIn,
    beforeEnter: (to, from, next)=>{
      if(!localStorage.getItem("access_token")) next();
      else next('/');
    }
  },
  {
    path: "/Auth/Register",
    name: "Register",
    component: Register
  },
  {
    path: "/Admin/Dashboard",
    name: "Admin_Dashboard",
    component: Admin_Dashboard,
    meta: {
      requiresAdminAuth : true
    }
  },
  {
    path: "/Admin/User_list",
    name: "Admin_ManageUser",
    component: ManageUsers,
    meta: {
      requiresAdminAuth : true
    }
  },
  {
    path: "/Admin/Order_list",
    name: "Order_list",
    component: Order_list,
    meta: {
      requiresAdminAuth : true
    }
  },
  {
    path: "/Admin/Product_list",
    name: "Product_list",
    component: Product_list,
    meta: {
      requiresAdminAuth : true
    }
  },
  {
    path: "/Admin/Add_product",
    name: "AddCustomProductList",
    component: AddCustomProductList,
    meta: {
      requiresAdminAuth : true
    }
  },
  {
    path: "/Admin/Category",
    name: "categories_list",
    component: categories_list,
    meta: {
      requiresAdminAuth : true
    }
  },
  {
    path: "/Admin/Promotion",
    name: "pomotion_list",
    component: manage_promotion,
    meta: {
      requiresAdminAuth : true
    }
  },
  {
    path: "/Admin/Promotion_Product/:code",
    name: "product_pomotion_list",
    component: manage_promotion_product,
    props: true,
    meta: {
      requiresAdminAuth : true
    }
  }
  ]
});

function check_user(){
  if( localStorage.getItem('access_token') && localStorage.getItem('mail') ){
    store.commit('setCurrentUser', localStorage.getItem('mail'))
    return true
  }else{
    return false
  }
}

function check_permission(){
  if( localStorage.getItem('permission')){

    if (store.state.permissionUser == null){
      re_check()
    }

    store.commit('setPermission', localStorage.getItem('permission') )

    return localStorage.getItem('permission')
  }else{
    return 'user'
  }
}

function re_check(){
  service.get_user_profile().subscribe(res => {
    if (!res['user_id']){
      localStorage.clear()
      this.$router.go(this.$router.currentRoute)
    }
  }),((err) => {
    console.log("err")
    localStorage.clear()
    this.$router.go(this.$router.currentRoute)
  })
}


 router.beforeEach(async (to, from, next) => {
  const requiresAuth      = to.matched.some(x => x.meta.requiresAuth)
  const requiresAdminAuth = to.matched.some(x => x.meta.requiresAdminAuth)
  const currentUser       = await  check_user()
  const permission        = await  check_permission()
  
  if ( requiresAuth || requiresAdminAuth && !currentUser ) {
      next('/Auth/SignIn')
  } else if (requiresAuth || requiresAdminAuth ) {
    if ( requiresAuth && !requiresAdminAuth && currentUser){
      next()
    } else {
      if ( requiresAdminAuth && currentUser && permission == 'admin'){
        next()
      }else {
        next('/')
      }
    } 
  } else {
      next()
  }
})

export default router

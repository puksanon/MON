import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    currentUser: null,
    userProfile: {},
    permissionUser: null,
    productList: [],
    productStore: [],
    cart: [],
  },

  mutations: {
    setCurrentUser(state, val) {
      state.currentUser = val;
    },
    setUserProfile(state, val) {
      state.userProfile = val;
    },
    setPermission(state, val) {
      state.permissionUser = val;
    },
    setProdectList(state, val) {
      state.productList = val;
    },
    setProdectStore(state, val) {
      state.productStore = val;
    },
    addtoCart(state, val) {
      state.cart.push(val);
      localStorage.setItem("cart", JSON.stringify(state.cart));
    },
    initCartStore(state) {
      if (localStorage.getItem("cart")) {
        state.cart = JSON.parse(localStorage.getItem("cart"));
      } else {
        localStorage.setItem("cart", JSON.stringify(state.cart));
      }
    },
    deleteCart(state, index) {
      state.cart.splice(index, 1);
      localStorage.setItem("cart", JSON.stringify(state.cart));
    },
  },

  getters: {
    productFilter: (state) => (name) => {
      return state.productStore.filter((product) => product.name === name);
    },
  },

  actions: {},
});
